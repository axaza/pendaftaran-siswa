<?php
require 'function.php';

if( isset($_POST["submit"]) ) {

if( daftar($_POST) > 0 ){
  echo " <script> alert ('data terverivikasi'); window.location.href = 'registrasiSiswa.php';
  </script>
  ";
}else{
  echo " <script> alert ('data gagal ditambahkan'); window.location.href = 'daftar.php';
  </script>
  ";
}
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.tambah.css">
    <title>DAFTAR</title>
    <style>
        .center {
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
        }
        
        form {
            max-width: 400px;
            padding: 20px;
            border: 1px solid #ccc;
            border-radius: 4px;
            background-color: #f2f2f2;
        }
    </style>
</head>
<body>
<h1>PENDAFTARAN SISWA BARU</h1>
    <nav>
   <a href="index.php"> HALAMAN UTAMA</a>
   <a href="">DAFTAR</a>
    <a href="logins.php">LOGIN SISWA</a> 
    <div class="animation start-home"></div>
    </nav>
    <br>
    <div style align="middle">
    <h1>pendaftaran</h1>

    <form action="" method="post">
    <ul>
         <li>
         <label for="nama">Nama:</label>
       <input type="text" name="nama" id="nama" required autocomplete="off">
         </li>
         <li>
         <label for="nama">NIK:</label>
       <input type="text" name="nisn" id="nisn" required autocomplete="off" minlength="10" maxlength="10">
         </li>
         <li>
         <label for="alamat">Alamat:</label>
       <input type="text" name="alamat" id="alamat" required autocomplete="off">
         </li>
         <li>
            jenis kelamin: <br>
         <input type="radio" name="jenis_kelamin" value="laki-laki"><label for="laki-laki" required>laki-laki</label> <br>
        <input type="radio" name="jenis_kelamin" value="perempuan"><label for="perempuan" required>perempuan</label>
        <?php if ($_SERVER["REQUEST_METHOD"] == "POST"){
          $Jenis_kelamin = $_POST["jenis_kelamin"];
          echo $Jenis_kelamin;
        } ?>
         </li> 
         <li>
         <label for="np_hp">No Hp:</label>
            <input type="text" name="no_hp" id="no_hp" autocomplete="off" required minlength="12" maxlength="15">
         </li>
         <li>
         <label for="email">Email:</label>
       <input type="text" name="email" id="email" autocomplete="off" required>
         </li> 
        <li>
            <button type="submit" name="submit">daftar</button>
        </li>
    </ul>
    </form>
    </div>
</body>
</html>
