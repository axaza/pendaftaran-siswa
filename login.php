<?php
 session_start();

 if( !isset ($_SESSION["login"]) ){
     header("location: logins.php");
      exit;
  }

require 'function.php';


if( isset($_POST["login"])){

    $username = $_POST["username"];
    $password = $_POST["password"];

$result = mysqli_query($conn , "SELECT * FROM user WHERE username = '$username'");

if( mysqli_num_rows($result) === 1){
    
        // cek password
        $row = mysqli_fetch_assoc($result);
        if( password_verify($password, $row["password"]) ){
     
            
            header("location: admin.php");
            
            exit;
        }
}   $error = true;
   }

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="login.css">
    <title>halaman login admin</title>
</head>
<body>
     <h1>PENDAFTARAN SISWA BARU</h1>
    <nav>
   <a href="index.php"> HALAMAN UTAMA</a>
   <a href="daftar.php">DAFTAR</a>
    <a href="logins.php">LOGIN SISWA</a> 
    <div class="animation start-home"></div>
    </nav>
    <br>
    <h1>Halaman Login ADMIN!</h1>
    <div style align="center"> 
        <form action="" method="post">
    <ul>
        <li>
            <label for="username">username :</label>
            <input type="text" name="username" id="username" autocomplete="off">
        </li>
        <li>
        <label for="password">password :</label>
            <input type="password" name="password" id="password" autocomplete="off">
        </li>
        <li>
            <input type="checkbox" name="remember" id="remember" >
            <label for="remember">ingatkan saya</label>
        </li>
        <li>
            <button type="submit" name="login">login</button>
        </li>
    </ul>
        </form>
    </div>
   
</body>
</html>