<?php
 session_start();

 if( !isset ($_SESSION["login"]) ){
     header("location: logins.php");
      exit;
  }
require 'function.php';

// ambil data dari url
$id = $_GET["id"];
// var_dump ($id);
// query data mahasiswa berdasarkan id

$mhs = query("SELECT * FROM mahasiswa WHERE id = $id" )[0];

// var_dump ($mhs);

if (isset($_POST["submit"])) {

  if (ubah($_POST) > 0) {
    echo " <script> alert('data berhasi diubah'); 
    window.location.href = 'admin.php';
    </script>
    ";
  } else {
    echo "<script> alert('data gagal diubah');
    window.location.href = 'admin.php';
    </script>
    ";
  }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>ubah data</title>
</head>

<body>
  <h1>Ubah Data Mahasiswa</h1>

  <form action="" method="post">
    <input type="hidden" name="id" value="<?= $mhs["id"]; ?>">
    <ul>
      <li>
        <label for="nama">Nama:</label>
        <input type="text" name="nama" id="nama" required autocomplete="off" value="<?= $mhs["nama"]; ?>" >
      </li>
      <li>
        <label for="nisn">NISN:</label>
        <input type="text" name="nisn" id="nisn" required value="<?= $mhs["nisn"]; ?>" minlength="10" maxlength="10">
      </li>
      <li>
        <label for="alamat">Alamat:</label>
        <input type="text" name="alamat" id="alamat" required autocomplete="off" value="<?= $mhs["alamat"]; ?>">
      </li>
      <li>
        jenis kelamin: <br>
        <input type="radio" name="jenis_kelamin" value="laki-laki" required><label for="laki-laki">laki-laki</label> <br>
        <input type="radio" name="jenis_kelamin" value="perempuan" required><label for="perempuan">perempuan</label>
        <?php
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
          $Jenis_kelamin = isset($_POST["jenis_kelamin"]) ? $_POST["jenis_kelamin"] : "";
          echo $Jenis_kelamin;
        }
        ?>
      </li>
      <li>
        <label for="no_hp">No Hp:</label>
        <input type="text" name="no_hp" id="no_hp" required value="<?= $mhs["no_hp"]; ?>" minlength="12" maxlength="15">
      </li>
      <li>
        <label for="email">Email:</label>
        <input type="text" name="email" id="email" autocomplete="off" required value="<?= $mhs["email"]; ?>">
        <button type="submit" name="submit">ubah</button>
      </li>
    </ul>
  </form>
</body>

</html>